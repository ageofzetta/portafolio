import { createLocalVue, RouterLinkStub, MountOptions } from '@vue/test-utils';
import Vuex, { StoreOptions, Store } from 'vuex';
import Vue from 'vue';
import Router, { Route, RouteConfig } from 'vue-router';
const localVue = createLocalVue();
localVue.filter('toMoney', (value) => value);
localVue.use(Vuex);
localVue.use(Router);
export const storeFactory = (
	storeOptions = ({
		state: {},
		getters: {},
		actions: {},
		mutations: {},
	} as StoreOptions<any>) as any,
): Store<any> => new Vuex.Store(storeOptions);
export const mockstoreFactory = () => ({ dispatch: jest.fn(() => Promise.resolve()) });
export interface IMountingOptions<V extends Vue> extends MountOptions<V> {
	store: any;
}
export const mountingOptionsFactory = (ProvidedRouter: Router | false = false): IMountingOptions<Vue> => ({
	sync: false,
	localVue,
	store: storeFactory() as Store<any>,
	provide: () => ({
		$validator: '',
	}),
	mocks: {
		$store: {},
		assets: () => 'assets',
		// $route,
	},
	stubs: {
		RouterLink: RouterLinkStub,
	},
	router: ProvidedRouter || new Router(),
});
