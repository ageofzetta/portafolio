import { IRootState } from './types';
import Vue from 'vue';
import Vuex, { Store, StoreOptions } from 'vuex';

Vue.use(Vuex);

const ApplicationStore: StoreOptions<IRootState> = {
	actions: {},
	getters: {},
	modules: {},
	mutations: {},
};
export default new Vuex.Store<IRootState>(ApplicationStore);
