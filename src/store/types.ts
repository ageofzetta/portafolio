import { StarWarsState } from '@/modules/StarWars/types';
import { PokemonState } from '@/modules/Pokemon/types';

// State Interfaces

export interface IRootState {
	StarWarsModule: StarWarsState;
	PokemonModule: PokemonState;
}
