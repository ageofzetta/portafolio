import Vue from 'vue';
import App from '@/App.vue';
import router from './router/index';
import store from './store/index';
import './registerServiceWorker';
import ErrorBoundary from '@/modules/Common/components/ErrorBoundary.vue';
import PortalVue from 'portal-vue';
import './hooks';

Vue.component('ErrorBoundary', ErrorBoundary);

(async () => {
	new Vue({
		router,
		store,
		render: (h) => h(App),
	}).$mount('#app');
})();

// TODO: alerts
// TODO: pwas (offline)
// TODO: service workers
// TODO: css grid in components
// TODO: docker (CI)
// TODO: graphQL
// TODO: aws cognito

// TODO: Leer manual de gitlab
// TODO: usar api de gitlab
// TODO: curso de algoritmos JS y entrevista de udemy

// TODO: Configure atapia.de with amplify
