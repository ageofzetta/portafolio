const enum AsUrlString {}
type UrlString = string & AsUrlString;

export interface PokeAPIBaseUnit {
	name: string;
	url: UrlString;
}
export interface Pokemon extends PokeAPIBaseUnit {
	id?: number;
	base_experience?: number;
	height?: number;
	is_default?: boolean;
	location_area_encounters?: string;
	abilities?: Array<{
		is_hidden: boolean;
		slot: number;
		ability: PokeAPIBaseUnit;
	}>;
	forms?: PokeAPIBaseUnit[];
	moves?: Array<{
		move: PokeAPIBaseUnit;
	}>;
	order?: number;
	species?: PokeAPIBaseUnit;
	sprites?: {
		back_default: null | string;
		back_female: null | string;
		back_shiny: null | string;
		back_shiny_female: null | string;
		front_default: null | string;
		front_female: null | string;
		front_shiny: null | string;
		front_shiny_female: null | string;
	};
	stats?: Array<{
		base_stat: number;
		effort: number;
		stat: PokeAPIBaseUnit;
	}>;
	types?: Array<{
		slot: number;
		type: PokeAPIBaseUnit;
	}>;
	selected: boolean;
}
export interface PokemonState {
	pokemon: Pokemon[];
}
export interface GetPokemonAPIResponse {
	count: number;
	next: UrlString | null;
	previous: UrlString | null;
	results: PokeAPIBaseUnit[];
}
