import { mountingOptionsFactory, storeFactory } from '@/testBootstrap';
import { shallowMount, createLocalVue, RouterLinkStub, config, MountOptions } from '@vue/test-utils';
import Vue from 'vue';
import PokemonComponent from '@/modules/Pokemon/views/Pokemon.vue';
import * as PokemonNamespaces from '@/modules/Pokemon/namespaces.ts';
import { Pokemon, PokemonState } from '../types';
import Router, { Route, RouteConfig } from 'vue-router';
import PokemonRoutes from '@/modules/Pokemon/router/index';
import PokemonStore from '@/modules/Pokemon/store';

config.logModifiedComponents = false;
const PokemonGuard = jest.fn(() => Promise.resolve());
const stubbedRoutes = PokemonRoutes.slice().map((route: RouteConfig) => {
	return {
		...route,
		beforeEnter: route.path === '/pokemon' ? PokemonGuard : route.beforeEnter,
	};
});
const stubbedRouter = new Router({
	routes: stubbedRoutes,
});
const stubbedStore = {
	...PokemonStore,
	state: {
		pokemon: [
			{
				name: 'charmander',
				url: 'https://pokeapi.co/api/v2/pokemon/4',
			},
			{
				name: 'pikachu',
				url: 'https://pokeapi.co/api/v2/pokemon/25',
			},
		],
	} as PokemonState,
};

describe('Pokemon.vue', () => {
	let mountingOptions: MountOptions<Vue>;
	beforeEach(() => {
		mountingOptions = mountingOptionsFactory(stubbedRouter);
		mountingOptions.store = storeFactory(stubbedStore);
	});

	it('Mounts Pokemon correctly', () => {
		const wrapper = shallowMount(PokemonComponent, mountingOptions);
		expect(wrapper).toBeDefined();
	});
	it('Should trigger guard', () => {
		stubbedRouter.push({ path: '/pokemon' });
		expect(PokemonGuard).toHaveBeenCalledTimes(1);
	});
});
