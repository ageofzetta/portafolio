import { mountingOptionsFactory, storeFactory } from '@/testBootstrap';
import { shallowMount, createLocalVue, RouterLinkStub, config, MountOptions } from '@vue/test-utils';
import Vue from 'vue';
import PokemonComponent from '@/modules/Pokemon/components/PokemonCard.vue';
import * as PokemonNamespaces from '@/modules/Pokemon/namespaces.ts';
import { PokemonState } from '@/modules/Pokemon/types';
import Router, { Route, RouteConfig } from 'vue-router';
import PokemonRoutes from '@/modules/Pokemon/router/index';
import PokemonStore from '@/modules/Pokemon/store';

config.logModifiedComponents = false;
const samplePokemon = [
	{
		name: 'charmander',
		url: 'https://pokeapi.co/api/v2/pokemon/4',
	},
	{
		name: 'pikachu',
		url: 'https://pokeapi.co/api/v2/pokemon/25',
	},
];
const SelectPokemon = jest.fn(() => Promise.resolve());
const PokemonGuard = jest.fn(() => Promise.resolve());
const stubbedRoutes = PokemonRoutes.slice().map((route: RouteConfig) => {
	return {
		...route,
		beforeEnter: route.path === '/pokemon' ? PokemonGuard : route.beforeEnter,
	};
});
const stubbedRouter = new Router({
	routes: stubbedRoutes,
});
const stubbedStore = {
	...PokemonStore,
	state: {
		pokemon: samplePokemon,
	} as PokemonState,
	actions: {
		[PokemonNamespaces.SELECT_POKEMON]: SelectPokemon,
	},
};

describe('Pokemon.vue', () => {
	let mountingOptions: MountOptions<Vue>;
	beforeEach(() => {
		mountingOptions = mountingOptionsFactory(stubbedRouter);
		mountingOptions.store = storeFactory(stubbedStore);
	});

	it('Mounts Component correctly', () => {
		const wrapper = shallowMount(PokemonComponent, {
			...mountingOptions,
			propsData: {
				pokemon: samplePokemon[0],
			},
		});
		expect(wrapper).toBeDefined();
	});
	it('Select a pokemon', () => {
		const wrapper = shallowMount(PokemonComponent, {
			...mountingOptions,
			propsData: {
				pokemon: samplePokemon[0],
			},
		});
		const card = wrapper.find('.card');
		card.trigger('click');
		expect(SelectPokemon).toHaveBeenCalledTimes(1);
	});
});
