import deepClone from 'lodash.clonedeep';
import { StoreOptions } from 'vuex';
import initialState from '@/modules/Pokemon/store/initialState';
import actions from '@/modules/Pokemon/store/actions';
import getters from '@/modules/Pokemon/store/getters';
import mutations from '@/modules/Pokemon/store/mutations';

export default {
	actions,
	getters,
	mutations,
	state: deepClone(initialState),
} as StoreOptions<any>;
