import { ActionTree, ActionContext } from 'vuex';
import logdown from 'logdown';
import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import { IRootState } from '@/store/types';
import * as PokemonNamespaces from '@/modules/Pokemon/namespaces';
import { PokemonState, GetPokemonAPIResponse, PokeAPIBaseUnit, Pokemon } from '../types';
import config from '@/config';

const logger = logdown('Pokémon:store:services', { prefixColor: '#ef5350', markdown: true });
logger.state.isEnabled = config.enableLogger && config.environment !== 'production';
const services: ActionTree<PokemonState, IRootState> = {
	[PokemonNamespaces.SERVICE_REQ_POKEMON]: async ({
		state,
	}: ActionContext<PokemonState, IRootState>): Promise<PokeAPIBaseUnit[]> => {
		logger.log('SERVICE REQ POKEMON');
		const requestConfiguration: AxiosRequestConfig = {
			method: 'GET',
			url: 'https://pokeapi.co/api/v2/pokemon/',
		};
		const response: AxiosResponse<GetPokemonAPIResponse> = await axios(requestConfiguration);
		const results: PokeAPIBaseUnit[] = response.data.results;
		return results;
	},
	[PokemonNamespaces.SERVICE_REQ_A_POKEMON]: async (
		{ state }: ActionContext<PokemonState, IRootState>,
		payload: Pokemon,
	): Promise<Pokemon> => {
		logger.log('SERVICE REQ A POKEMON');
		const requestConfiguration: AxiosRequestConfig = {
			method: 'GET',
			url: payload.url,
		};
		const response: AxiosResponse<Pokemon> = await axios(requestConfiguration);
		const results: Pokemon = response.data;
		return { ...results, url: payload.url } as Pokemon;
	},
};

export default services;
