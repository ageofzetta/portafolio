import logdown from 'logdown';
import { GetterTree } from 'vuex';
import { IRootState } from '@/store/types';
import * as PokemonNamespaces from '@/modules/Pokemon/namespaces';
import { PokemonState, Pokemon } from '@/modules/Pokemon/types';

const getters: GetterTree<PokemonState, IRootState> = {
	[PokemonNamespaces.GET_POKEMON]: (state: PokemonState): Pokemon[] => {
		return state.pokemon;
	},
	[PokemonNamespaces.GET_SELECTED_POKEMON]: (state: PokemonState): Pokemon | null => {
		return (
			state.pokemon.find((pokemon: Pokemon) => {
				return pokemon.selected;
			}) || null
		);
	},
};

export default getters;
