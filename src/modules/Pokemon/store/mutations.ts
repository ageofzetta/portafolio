import logdown from 'logdown';
import Vue from 'vue';
import { MutationTree } from 'vuex';
import * as PokemonNamespaces from '@/modules/Pokemon/namespaces';
import { PokemonState, Pokemon } from '@/modules/Pokemon/types';
import config from '@/config';

const logger = logdown('Pokémon:store:mutations', { prefixColor: '#ef5350', markdown: true });
logger.state.isEnabled = config.enableLogger && config.environment !== 'production';

const mutations: MutationTree<PokemonState> = {
	[PokemonNamespaces.MTT_POKEMON]: (state: PokemonState, payload: Pokemon[]): void => {
		logger.log('MTT_POKEMON');
		const compare = (a: Pokemon, b: Pokemon) => {
			if (a.name < b.name) {
				return -1;
			}
			if (a.name > b.name) {
				return 1;
			}
			return 0;
		};
		Vue.set(state, 'pokemon', payload.slice().sort(compare) as Pokemon[]);
	},
};

export default mutations;
