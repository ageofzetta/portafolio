import { ActionTree, ActionContext } from 'vuex';
import logdown from 'logdown';
import { IRootState } from '@/store/types';
import * as PokemonNamespaces from '@/modules/Pokemon/namespaces';
import { PokemonState, PokeAPIBaseUnit, Pokemon } from '../types';
import services from '@/modules/Pokemon/store/services';
import config from '@/config';

const logger = logdown('Pokémon:store:actions', { prefixColor: '#ef5350', markdown: true });
logger.state.isEnabled = config.enableLogger && config.environment !== 'production';
const actions: ActionTree<PokemonState, IRootState> = {
	...services,
	[PokemonNamespaces.ORQST_REFRESH_POKEMON]: async ({
		state,
		dispatch,
		commit,
	}: ActionContext<PokemonState, IRootState>): Promise<PokemonState> => {
		logger.log('***ORQST REFRESH POKEMON***');
		let allPokemon: Pokemon[] = await dispatch(PokemonNamespaces.SERVICE_REQ_POKEMON);
		allPokemon = allPokemon.map((pokemon: Pokemon) => {
			return {
				...pokemon,
				selected: false,
			} as Pokemon;
		});
		commit(PokemonNamespaces.MTT_POKEMON, allPokemon as Pokemon[]);
		return state;
	},
	[PokemonNamespaces.SELECT_POKEMON]: async (
		{ state, dispatch, commit, getters }: ActionContext<PokemonState, IRootState>,
		payload: Pokemon | null,
	): Promise<PokemonState> => {
		logger.log('***SELECT POKEMON***');
		const pokemonArray: Pokemon[] = getters[PokemonNamespaces.GET_POKEMON];
		let newPokemonArray = pokemonArray.map((pokemon) => {
			return {
				...pokemon,
				selected: false,
			};
		});
		if (payload) {
			const slctdPokemon: Pokemon = payload.hasOwnProperty('id')
				? payload
				: await dispatch(PokemonNamespaces.SERVICE_REQ_A_POKEMON, payload as Pokemon);
			newPokemonArray = newPokemonArray
				.filter((pokemon: Pokemon) => {
					return pokemon.name !== slctdPokemon.name;
				})
				.concat([{ ...slctdPokemon, selected: true }]);
		}
		commit(PokemonNamespaces.MTT_POKEMON, newPokemonArray as Pokemon[]);
		return state;
	},
};

export default actions;
