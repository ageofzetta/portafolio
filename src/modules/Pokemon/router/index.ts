import { RouteConfig } from 'vue-router';
import multiguard from 'vue-router-multiguard';
import * as guards from './guards';
const meta = { required: ['guest'] };

// TODO: do unit testing of router

const PokemonRoutes: RouteConfig[] = [
	{
		meta,
		path: '/pokemon',
		name: 'Pokemon Home',
		component: () => import(/* webpackChunkName: "PokemonModule" */ '@/modules/Pokemon/views/Pokemon.vue'),
		beforeEnter: multiguard([guards.fetchPokemon]),
	},
];

export default PokemonRoutes;
