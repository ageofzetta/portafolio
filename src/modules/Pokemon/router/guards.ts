import store from '@/store';
import * as PokemonNamespaces from '@/modules/Pokemon/namespaces';
import PokemonModule from '@/modules/Pokemon/store';

export const fetchPokemon = async (to, from, next) => {
	await store.registerModule('PokemonModule', PokemonModule);
	await store.dispatch(PokemonNamespaces.ORQST_REFRESH_POKEMON);
	/*
	PokeAPI fetch
	*/
	return next();
};
