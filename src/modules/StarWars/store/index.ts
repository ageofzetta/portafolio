import deepClone from 'lodash.clonedeep';
import logdown from 'logdown';
import Vue from 'vue';
import { ActionContext, ActionTree, GetterTree, MutationTree, StoreOptions } from 'vuex';
import { IRootState } from '@/store/types';
import * as StarwarsNamespaces from '@/modules/StarWars/namespaces';
import { StarWarsState, SWCharacter, SWPlanet, SWFilm } from '@/modules/StarWars/types';
import initialState from '@/modules/StarWars/store/initialState';
import services from '@/modules/StarWars/store/services';
import config from '@/config';

const logger = logdown('StarWars:store:', { prefixColor: '#6149fb', markdown: true });
logger.state.isEnabled = config.enableLogger && config.environment !== 'production';
const StarWarsStoreState: StarWarsState = deepClone(initialState);
const StarWarsStoreGetters: GetterTree<StarWarsState, IRootState> = {
	[StarwarsNamespaces.GET_CHARACTERS]: (state: StarWarsState): SWCharacter[] => {
		logger.log('GET_CHARACTERS');
		return state.characters;
	},
	[StarwarsNamespaces.GET_SELECTED_CHARACTERS]: (state: StarWarsState): SWCharacter[] => {
		logger.log('GET_CHARACTERS');
		return state.characters.filter((character: SWCharacter) => character.selected);
	},
	[StarwarsNamespaces.GET_PLANETS]: (state: StarWarsState): SWPlanet[] => {
		logger.log('GET_PLANETS');
		return state.planets;
	},
	[StarwarsNamespaces.GET_FILMS]: (state: StarWarsState): SWFilm[] => {
		logger.log('GET_FILMS');
		return state.films;
	},
};
const StarWarsStoreMutations: MutationTree<StarWarsState> = {
	[StarwarsNamespaces.MTT_CHARACTERS]: (state: StarWarsState, payload: SWCharacter[]): void => {
		logger.log('MTT_CHARACTERS');
		const compare = (a: SWCharacter, b: SWCharacter) => {
			if (a.name < b.name) {
				return -1;
			}
			if (a.name > b.name) {
				return 1;
			}
			return 0;
		};
		Vue.set(state, 'characters', payload.slice().sort(compare) as SWCharacter[]);
	},
	[StarwarsNamespaces.MTT_PLANETS]: (state: StarWarsState, payload: SWPlanet[]): void => {
		logger.log('MTT_PLANETS');
		Vue.set(state, 'planets', payload.slice() as SWPlanet[]);
	},
	[StarwarsNamespaces.MTT_FILMS]: (state: StarWarsState, payload: SWFilm[]): void => {
		logger.log('MTT_FILMS');
		Vue.set(state, 'films', payload.slice() as SWFilm[]);
	},
};
const StarWarsStoreActions: ActionTree<StarWarsState, IRootState> = {
	...services,
	[StarwarsNamespaces.ORQST_REFRESH_CHARACTERS]: async ({
		state,
		dispatch,
		commit,
	}: ActionContext<StarWarsState, IRootState>): Promise<StarWarsState> => {
		logger.log('***ORQST REFRESH CHARACTERS***');
		const starWarsCharacters: SWCharacter[] = await dispatch(StarwarsNamespaces.SERVICE_REQ_CHARACTERS);
		commit(StarwarsNamespaces.MTT_CHARACTERS, starWarsCharacters as SWCharacter[]);
		return state;
	},
	[StarwarsNamespaces.ORQST_REFRESH_PLANETS]: async ({
		state,
		dispatch,
		commit,
	}: ActionContext<StarWarsState, IRootState>): Promise<StarWarsState> => {
		logger.log('***ORQST REFRESH PLANETS***');
		const starWarsPlanets: SWPlanet[] = await dispatch(StarwarsNamespaces.SERVICE_REQ_PLANETS);
		commit(StarwarsNamespaces.MTT_PLANETS, starWarsPlanets as SWPlanet[]);
		return state;
	},
	[StarwarsNamespaces.ORQST_REFRESH_FILMS]: async ({
		state,
		dispatch,
		commit,
	}: ActionContext<StarWarsState, IRootState>): Promise<StarWarsState> => {
		logger.log('***ORQST REFRESH FILMS***');
		const starWarsFilms: SWFilm[] = await dispatch(StarwarsNamespaces.SERVICE_REQ_FILMS);
		commit(StarwarsNamespaces.MTT_FILMS, starWarsFilms as SWFilm[]);
		return state;
	},
	[StarwarsNamespaces.SELECT_CHARACTER]: async (
		{ state, dispatch, commit, getters }: ActionContext<StarWarsState, IRootState>,
		payload: SWCharacter,
	): Promise<StarWarsState> => {
		logger.log('***SELECT_CHARACTER***');
		const selectedCharacter = await dispatch(StarwarsNamespaces.SERVICE_REQ_CHARACTER, payload as SWCharacter);
		const characterArray: SWCharacter[] = deepClone(getters[StarwarsNamespaces.GET_CHARACTERS]);
		const newCharacterArray: SWCharacter[] = characterArray
			.map((character: SWCharacter) => {
				return {
					...character,
					selected: false,
				};
			})
			.filter((character: SWCharacter) => {
				return character.name !== selectedCharacter.name;
			})
			.concat([selectedCharacter]);
		commit(StarwarsNamespaces.MTT_CHARACTERS, newCharacterArray as SWCharacter[]);
		return state;
	},
};
export default {
	actions: StarWarsStoreActions,
	getters: StarWarsStoreGetters,
	mutations: StarWarsStoreMutations,
	state: StarWarsStoreState,
} as StoreOptions<any>;
