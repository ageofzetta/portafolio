import { StarWarsState } from '@/modules/StarWars/types';

export default {
	characters: [],
	planets: [],
	films: [],
} as StarWarsState;
