import { ActionTree, ActionContext } from 'vuex';
import logdown from 'logdown';
import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import { IRootState } from '@/store/types';
import * as StarwarsNamespaces from '@/modules/StarWars/namespaces';
import {
	StarWarsState,
	SWCharacter,
	SWCharactersAPIResponse,
	SWCharacterFromAPI,
	SWPlanetAPIResponse,
	SWPlanet,
	SWPlanetFromAPI,
	SWFilmAPIResponse,
	SWFilm,
	SWFilmFromAPI,
} from '@/modules/StarWars/types';
import config from '@/config';

const logger = logdown('StarWars:services:', { prefixColor: '#f39c12', markdown: false });
logger.state.isEnabled = config.enableLogger && config.environment !== 'production';
const services: ActionTree<StarWarsState, IRootState> = {
	[StarwarsNamespaces.SERVICE_REQ_CHARACTERS]: async ({
		state,
	}: ActionContext<StarWarsState, IRootState>): Promise<SWCharacter[]> => {
		logger.log('SERVICE_REQ_CHARACTERS');
		const requestConfiguration: AxiosRequestConfig = {
			method: 'GET',
			url: 'https://swapi.co/api/people/',
		};
		const response: AxiosResponse<SWCharactersAPIResponse> = await axios(requestConfiguration);
		const results: SWCharacter[] = response.data.results.map(
			(person: SWCharacterFromAPI): SWCharacter => {
				const {
					name,
					height,
					mass,
					hair_color,
					skin_color,
					eye_color,
					birth_year,
					gender,
					homeworld,
					films,
					species,
					vehicles,
					starships,
					url,
				} = person;
				return {
					name,
					height: parseInt(height, 10),
					mass: parseInt(mass, 10),
					hair_color,
					skin_color,
					eye_color,
					birth_year,
					gender,
					homeworld,
					films,
					species,
					vehicles,
					starships,
					url,
					selected: false,
				} as SWCharacter;
			},
		);
		return results;
	},
	[StarwarsNamespaces.SERVICE_REQ_CHARACTER]: async (
		{ state }: ActionContext<StarWarsState, IRootState>,
		payload: SWCharacter,
	): Promise<SWCharacter> => {
		logger.log('SERVICE_REQ_CHARACTERS');
		const requestConfiguration: AxiosRequestConfig = {
			method: 'GET',
			url: payload.url,
		};
		const response: AxiosResponse<SWCharacterFromAPI> = await axios(requestConfiguration);
		const {
			name,
			height,
			mass,
			hair_color,
			skin_color,
			eye_color,
			birth_year,
			gender,
			homeworld,
			films,
			species,
			vehicles,
			starships,
			url,
		} = response.data;
		return {
			name,
			height: parseInt(height, 10),
			mass: parseInt(mass, 10),
			hair_color,
			skin_color,
			eye_color,
			birth_year,
			gender,
			homeworld,
			films,
			species,
			vehicles,
			starships,
			url,
			selected: true,
		} as SWCharacter;
	},
	[StarwarsNamespaces.SERVICE_REQ_PLANETS]: async ({
		state,
	}: ActionContext<StarWarsState, IRootState>): Promise<SWPlanet[]> => {
		logger.log('SERVICE_REQ_PLANETS');
		const requestConfiguration: AxiosRequestConfig = {
			method: 'GET',
			url: 'https://swapi.co/api/planets/',
		};
		const response: AxiosResponse<SWPlanetAPIResponse> = await axios(requestConfiguration);
		const results: SWPlanet[] = response.data.results.map(
			(planet: SWPlanetFromAPI): SWPlanet => {
				const {
					name,
					rotation_period,
					orbital_period,
					diameter,
					climate,
					gravity,
					terrain,
					surface_water,
					population,
					residents,
					films,
					url,
				} = planet;
				return {
					name,
					rotation_period: parseInt(rotation_period, 10),
					orbital_period: parseInt(orbital_period, 10),
					diameter: parseInt(diameter, 10),
					climate,
					gravity,
					terrain,
					surface_water: parseInt(surface_water, 10),
					population: parseInt(population, 10),
					residents,
					films,
					url,
				} as SWPlanet;
			},
		);
		return results;
	},
	[StarwarsNamespaces.SERVICE_REQ_FILMS]: async ({
		state,
	}: ActionContext<StarWarsState, IRootState>): Promise<SWFilm[]> => {
		logger.log('SERVICE_REQ_FILMS');
		const requestConfiguration: AxiosRequestConfig = {
			method: 'GET',
			url: 'https://swapi.co/api/planets/',
		};
		const response: AxiosResponse<SWFilmAPIResponse> = await axios(requestConfiguration);
		const results: SWFilm[] = response.data.results.map(
			(film: SWFilmFromAPI): SWFilm => {
				const {
					title,
					episode_id,
					opening_crawl,
					director,
					producer,
					release_date,
					characters,
					planets,
					starships,
					vehicles,
					species,
					url,
				} = film;
				return {
					title,
					episode_id,
					opening_crawl,
					director,
					producer,
					release_date,
					characters,
					planets,
					starships,
					vehicles,
					species,
					url,
				} as SWFilm;
			},
		);
		return results;
	},
};

export default services;
