import store from '@/store';
import * as StarWarsNamespaces from '@/modules/StarWars/namespaces';
import StarWarsModule from '@/modules/StarWars/store';

export const fetchCharacters = async (to, from, next) => {
	await store.registerModule('StarWarsModule', StarWarsModule);
	await store.dispatch(StarWarsNamespaces.ORQST_REFRESH_CHARACTERS);
	// await store.dispatch(StarWarsNamespaces.ORQST_REFRESH_PLANETS);
	// await store.dispatch(StarWarsNamespaces.ORQST_REFRESH_FILMS);
	return next();
};
