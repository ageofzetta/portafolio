import { RouteConfig } from 'vue-router';
import multiguard from 'vue-router-multiguard';
import * as guards from './guards';
const meta = { required: ['guest'] };

// TODO: do unit testing of router

const StarWarsRoutes: RouteConfig[] = [
	{
		meta,
		path: '/star-wars',
		name: 'Star Wars Home',
		component: () => import(/* webpackChunkName: "StarWarsModule" */ '@/modules/StarWars/views/StarWars.vue'),
		beforeEnter: multiguard([guards.fetchCharacters]),
	},
];

export default StarWarsRoutes;
