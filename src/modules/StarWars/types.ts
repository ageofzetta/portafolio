const enum AsUrlString {}
type UrlString = string & AsUrlString;

export interface SWAPIResponse {
	count: number;
	next: string | null;
	previous: string | null;
}
export interface SWCharacterBaseModel {
	name: string;
	height: string | number;
	mass: string | number;
	hair_color: string;
	skin_color: string;
	eye_color: string;
	birth_year: string;
	gender: string;
	homeworld: string;
	films: UrlString[];
	species: UrlString[];
	vehicles: UrlString[];
	starships: UrlString[];
	url: UrlString;
}
export interface SWCharacter extends SWCharacterBaseModel {
	height: number;
	mass: number;
	selected: boolean;
}
export interface SWCharacterFromAPI extends SWCharacterBaseModel {
	height: string;
	mass: string;
	created: string;
	edited: string;
}
export interface SWCharactersAPIResponse extends SWAPIResponse {
	results: SWCharacterFromAPI[];
}
export interface SWPlanetBaseModel {
	name: string;
	rotation_period: number | string;
	orbital_period: number | string;
	diameter: number | string;
	climate: string;
	gravity: string;
	terrain: string;
	surface_water: number | string;
	population: number | string;
	residents: UrlString[];
	films: UrlString[];
	url: UrlString;
}
export interface SWPlanet extends SWPlanetBaseModel {
	rotation_period: number;
	orbital_period: number;
	diameter: number;
	surface_water: number;
	population: number;
}

export interface SWPlanetFromAPI extends SWPlanetBaseModel {
	rotation_period: string;
	orbital_period: string;
	diameter: string;
	surface_water: string;
	population: string;
	created: string;
	edited: string;
}
export interface SWPlanetAPIResponse extends SWAPIResponse {
	results: SWPlanetFromAPI[];
}
export interface SWFilmBaseModel {
	title: string;
	episode_id: number;
	opening_crawl: string;
	director: string;
	producer: string;
	release_date: string;
	characters: UrlString[];
	planets: UrlString[];
	starships: UrlString[];
	vehicles: UrlString[];
	species: UrlString[];
	url: string;
}
export interface SWFilmFromAPI extends SWFilmBaseModel {
	created: string;
	edited: string;
}
export type SWFilm = SWFilmBaseModel;

export interface SWFilmAPIResponse extends SWAPIResponse {
	results: SWFilmFromAPI[];
}

export interface StarWarsState {
	characters: SWCharacter[];
	planets: SWPlanet[];
	films: SWFilm[];
}
