import Vue from 'vue';
import store from '@/store';
import Router, { Route, RouteConfig } from 'vue-router';
import StarWarsRoutes from '@/modules/StarWars/router/index';
import PokemonRoutes from '@/modules/Pokemon/router/index';

Vue.use(Router);
const router = new Router({
	mode: 'history',
	routes: [...StarWarsRoutes, ...PokemonRoutes],
});
const globalGuard = (to: Route, from: Route, next) => {
	if (!!store.state.StarWarsModule) {
		store.unregisterModule('StarWarsModule');
	}
	if (!!store.state.PokemonModule) {
		store.unregisterModule('PokemonModule');
	}
	next();
};
router.beforeEach(globalGuard);

export default router;
