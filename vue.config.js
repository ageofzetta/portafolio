const path = require('path');
const webpack = require('webpack');
const BrotliPlugin = require('brotli-webpack-plugin');

const ManifestPlugin = require('webpack-manifest-plugin');

const result = require('dotenv').config({ path: path.resolve(__dirname, '.env') });
const environment = process.env.NODE_ENV || 'production';
const ENABLE_MAIN_LOGGER =
	process.env.ENABLE_MAIN_LOGGER === 'true' || process.env.ENABLE_MAIN_LOGGER === true ? true : false;
const USE_MOCKED_APIS = process.env.USE_MOCKED_APIS === 'true' || process.env.USE_MOCKED_APIS === true ? true : false;

const configObject = {
	publicPath: '/',
	outputDir: path.resolve(__dirname, 'dist'),
	filenameHashing: true,
	productionSourceMap: true,
	devServer: {
		// proxy: webappApi,
		port: 8081,
	},
	configureWebpack: {
		// devtool: '',
		devServer: {
			// proxy: webappApi,
		},
		plugins: [
			new ManifestPlugin({
				fileName: 'rev-manifest.json',
				seed: {
					name: 'sw',
				},
			}),
			new webpack.DefinePlugin({
				ENVIRONMENT: JSON.stringify(environment),
				ENABLE_MAIN_LOGGER: ENABLE_MAIN_LOGGER,
				USE_MOCKED_APIS: USE_MOCKED_APIS,
			}),
		],
	},
};
if (environment === 'production') {
	configObject.configureWebpack.plugins.push(
		new BrotliPlugin({
			asset: '[path].br[query]',
			test: /\.(js|css|html|svg)$/,
			threshold: 1,
			minRatio: 0.8,
		}),
	);
}
module.exports = configObject;
