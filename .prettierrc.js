module.exports = {
	singleQuote: true,
	printWidth: 120,
	trailingComma: 'all',
	bracketSpacing: true,
	arrowParens: 'always',
	useTabs: true,
};
